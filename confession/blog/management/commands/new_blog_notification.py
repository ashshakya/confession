from django.conf import settings
from django.core.management.base import BaseCommand

from confession.blog.models import Post
from confession.pubsub.models import Subscribers
from confession.utils import email


class Command(BaseCommand):
    """
        Usage: ./manage.py new_blog_notification
    """

    help = 'Send new blog notification to subscribed users'

    def handle(self, *args, **options):
        data = {}
        blogs = []
        posts = Post.objects.filter(
            is_approved=True
        ).order_by('-published_date')[:3]
        for index, post in enumerate(posts, start=1):
            # import ipdb;ipdb.set_trace()
            content = {
                'title_image': settings.DOMAIN + post.title_image.url,
                'title': post.title,
                'short_desc': post.text[:155],
                'author': post.owner.get_full_name(),
                'read_link': f'{settings.DOMAIN}/blog-detail/{post.pk}'
            }
            if index == 1:
                data.update({
                    'main': content
                })
            else:
                blogs.append(content)
            data.update({'blogs': blogs})
        for subscriber in Subscribers.objects.filter(is_active=True):
            email.send_from_template(
                to=subscriber.email,
                subject='Confession Notification: New blog',
                template='blog_notification_email.html',
                context={'data': data}
            )

