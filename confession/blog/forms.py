from django import forms

from confession.blog.models import *


class CommentForm(forms.ModelForm):
    comment = forms.CharField(label="", help_text="",
                              strip=True, widget=forms.Textarea())

    class Meta:
        model = Comment
        fields = ('comment',)
