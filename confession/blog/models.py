from django.contrib.auth import get_user_model
from django.db import models
from simple_history.models import HistoricalRecords
from ckeditor_uploader.fields import RichTextUploadingField

from confession.base.models import TimeStampModel
from confession.data.models import upload_image


UserModel = get_user_model()


class Post(TimeStampModel):
    owner = models.ForeignKey(
        UserModel, on_delete=models.CASCADE, related_name='posts'
    )
    title = models.CharField(max_length=255)
    title_image = models.ImageField(
        upload_to=upload_image, blank=True
    )
    text = RichTextUploadingField()
    published_date = models.DateTimeField(auto_now_add=True)
    is_approved = models.BooleanField(default=False)
    is_deleted = models.BooleanField(default=False)
    history = HistoricalRecords(
        history_change_reason_field=models.TextField(null=True)
    )

    def __str__(self):
        return self.title

    def delete(self):
        self.is_deleted = True
        self.save()

    def save(self, *args, **kwargs):
        self.full_clean()  # performs regular validation then clean()
        super(Post, self).save(*args, **kwargs)

    def clean(self):
        self.text = self.text.strip()

    class Meta:
        ordering = ['-created_at']


class Comment(TimeStampModel):
    user = models.ForeignKey(
        UserModel, on_delete=models.CASCADE, related_name='comment'
    )
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    comment = models.TextField()
    reply = models.ForeignKey(
        'self', blank=True, null=True, related_name='response',
        on_delete=models.CASCADE
    )

    def __str__(self):
        return f'{self.user}-{self.pk}'

    class Meta:
        ordering = ['-created_at']
