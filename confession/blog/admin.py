from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin

from confession.blog.models import Post


class ChangedDataSimpleHistoryAdmin(SimpleHistoryAdmin):
    """
    Custom history settings for displaying changed fields and changed values
    """
    history_list_display = ['changed_fields', 'changed_values']

    def changed_fields(self, obj):
        """
        Function for getting the changed fields
        """
        if obj.prev_record:
            delta = obj.diff_against(obj.prev_record)
            return delta.changed_fields
        return None

    def changed_values(self, obj):
        """
        Function for getting the changed values with respective fields
        """
        changed_fields = self.changed_fields(obj) or []
        changed_values = {}
        for fields in changed_fields:
            changed_values.update({
                fields: [
                    obj.__getattribute__(fields),
                    obj.prev_record.__getattribute__(fields)
                ]
            })
        return changed_values


@admin.register(Post)
class PostAdmin(ChangedDataSimpleHistoryAdmin):
    list_display = ('owner', 'title', 'published_date', 'is_approved')
    raw_id_fields = ('owner',)
    list_filter = ('is_approved', 'is_deleted',)

    search_fields = (
        'owner__first_name', 'owner__last_name', 'owner__email',
        'title'
    )
