import datetime

from rest_framework import serializers
from django.contrib.auth import get_user_model

from confession.blog.models import Post, Comment
from confession.notifications.models import Notification

User = get_user_model()


class PostApprovalFormSerializer(serializers.ModelSerializer):
    is_approve = serializers.BooleanField(help_text="Boolean value")

    class Meta:
        model = Post
        fields = ('is_approve',)


class BlogSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField('get_owner')
    published_date = serializers.SerializerMethodField()
    short_description = serializers.SerializerMethodField()

    class Meta:
        model = Post
        fields = (
            'id',
            'title',
            'title_image',
            'text',
            'short_description',
            'published_date',
            'is_approved',
            'is_deleted',
            'owner'
        )

    def get_owner(self, obj):
        return obj.owner.get_full_name()

    def get_published_date(self, obj):
        """
        return date in string format e.g 01 July, 2020
        """
        return datetime.datetime.strftime(obj.published_date, '%d %B, %Y')

    def get_short_description(self, obj):
        return obj.text[:500]


class BlogCreateSerializer(serializers.ModelSerializer):

    title = serializers.CharField(
        max_length=255,
        trim_whitespace=True,
        help_text="Title of blog"
    )
    text = serializers.CharField(
        allow_blank=False,
        trim_whitespace=True,
        help_text="Text/Content of blog"
    )
    owner_id = serializers.ModelField(
        model_field=User()._meta.get_field('id'),
        required=False,
        help_text="UserId of current user. (optional)"
    )
    request_from = serializers.CharField(
        max_length=10,
        default='api',
        read_only=True
    )

    def create(self, validated_data):
        validated_data['request_from'] = 'api'
        post = Post.objects.create(**validated_data)
        return post

    class Meta:
        model = Post
        fields = ('title', 'text', 'owner_id', 'request_from')


class CommentSerializer(serializers.ModelSerializer):

    comment = serializers.CharField(
        allow_blank=False,
        trim_whitespace=True,
        help_text="String value"
    )
    blog_id = serializers.ModelField(
        model_field=Post()._meta.get_field('id'),
        help_text="blog Id on which comment will apply"
    )
    user_id = serializers.ModelField(
        model_field=User()._meta.get_field('id'),
        read_only=True
    )

    def create(self, validated_data):
        comment = Comment.objects.create(**validated_data)
        return comment

    class Meta:
        model = Comment
        fields = ('comment', 'blog_id', 'user_id')


# class ReplySerializer(serializers.ModelSerializer):

#     reply = serializers.CharField(
#         allow_blank=False,
#         trim_whitespace=True,
#         help_text="String value"
#     )
#     which_comment_id = serializers.ModelField(
#         model_field=Comment()._meta.get_field('id'),
#         help_text="CommentID on which user reply "
#     )
#     blog_id = serializers.ModelField(
#         model_field=Post()._meta.get_field('id'),
#         help_text="blog Id on which reply will apply"
#     )
#     user_id = serializers.ModelField(
#         model_field=User()._meta.get_field('id'),
#         read_only=True
#     )

#     def create(self, validated_data):
#         reply = Reply.objects.create(**validated_data)
#         return reply

#     class Meta:
#         model = Reply
#         fields = ('reply', 'which_comment_id', 'blog_id', 'user_id')


class NotificationSerializer(serializers.ModelSerializer):

    unread = serializers.BooleanField(
        default=False,
        help_text="Boolean value for read status"
    )

    class Meta:
        model = Notification
        fields = '__all__'
        read_only_fields = ('verb', 'object_id', 'description',
                            'deleted', 'sender', 'content_type', 'blog')
