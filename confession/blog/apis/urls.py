from django.urls import path
# from rest_framework.documentation import include_docs_urls

from confession.blog.apis.blog_views import (
    BlogListView,
    CreateBlogView,
    PostListView,
    BlogDetailView,
    PostApprovalListView,
    PostApprovalFormView,
    CommentView,
    NotificationView,
)


post_detail = BlogDetailView.as_view({
    'get': 'retrieve',
    'put': 'update',
    'delete': 'destroy'

})
post_list = PostListView.as_view({'get': 'list'})
post_approval_list = PostApprovalListView.as_view({'get': 'list'})

post_approve = PostApprovalFormView.as_view({'put': 'update'})
notification = NotificationView.as_view({
    'get': 'list',
    'put': 'update'
})

urlpatterns = [
    # path('docs', include_docs_urls(
    #     title='Blogs API Docs'
    # )),

    path('all-blogs', BlogListView.as_view({'get': 'list'}), name='allblogs'),
    path('create-blog', CreateBlogView.as_view({'post': 'create'}), name='api-create-blog'),
    path('myblog', post_list, name='api-myblog'),
    path(
        '<int:pk>',
        post_detail,
        name='api-blog-operation'
    ),
    path('approval-list', post_approval_list, name='api-approval-list'),
    path('approve/<int:pk>', post_approve, name='api-approve'),

    path('comment', CommentView.as_view({'post': 'create'}), name='api-comment'),
    path('notification', notification, name='api-notification'),
]
