from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.http import HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.views.generic import (
    CreateView,
    DetailView,
    ListView,
    UpdateView,
    DeleteView,
)
from django.views.generic.edit import FormMixin, FormView
from django.urls import reverse_lazy
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from django.conf import settings

from confession.blog.decorators import editor_required, chief_required
from confession.blog.forms import *
from confession.blog.models import *
from confession.notifications.signals import send, recieve


@method_decorator([login_required], name='dispatch')
class CreatePostView(CreateView):
    """
    Create new blog
    """
    model = Post
    fields = ('title', 'text', )
    template_name = 'blog/add_post_form.html'
    success_url = 'post_list'

    def form_valid(self, form):
        post = form.save(commit=False)
        post.owner = self.request.user
        post.save()
        return redirect('post_list')


# @method_decorator([login_required], name='dispatch')
class PostListView(ListView):
    '''
    Show loggedIn user's post
    '''
    ordering = ('published_date',)
    context_object_name = 'post_list'
    template_name = 'blog/homepage.html'
    paginate_by = 10

    def get_queryset(self):
        # user = User.objects.get(username=self.request.user)
        # print(user)
        # import ipdb;ipdb.set_trace()
        return Post.objects.filter(
            # owner=self.request.user.id,
            is_deleted=False
        )

    def get_context_data(self, **kwargs):
        context = super(PostListView, self).get_context_data(**kwargs)
        # context['users'] = False
        # context['notifications'], context['unread_count'] = recieve(self.request.user)
        return context


# @method_decorator([login_required], name='dispatch')
class PostDetailView(FormMixin, DetailView):
    model = Post
    form_class = CommentForm
    template_name = 'blog/post_detail.html'

    def get_success_url(self):
        return reverse_lazy('post_detail', kwargs={'pk': self.object.pk})

    def get_context_data(self, **kwargs):
        context = super(PostDetailView, self).get_context_data(**kwargs)
        comments = Comment.objects.filter(blog_id=self.get_object().id).order_by('-comment_datetime')[:5]
        context['notifications'], context['unread_count'] = recieve(self.request.user)
        # provide the columns name in values_list
        context['comments'] = comments.values_list(
            'user_id__username', 'comment', 'id')
        context['reply'] = Reply.objects.filter(blog_id=self.object.pk)
        context['form'] = CommentForm(initial={'post': self.object})
        return context

    @method_decorator(login_required, name='dispatch')
    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        # Here, we would record the user's interest using the message
        # passed in form.cleaned_data['message']
        comment = form.save(commit=False)
        comment.blog = self.get_object()
        comment.user = self.request.user
        form.save()
        return super(PostDetailView, self).form_valid(form)


@method_decorator([login_required], name='dispatch')
class PostUpdateView(UpdateView):
    template_name = 'blog/add_post_form.html'
    model = Post
    fields = ['title', 'text']
    success_url = 'blog/post_list'

    def get_context_data(self, **kwargs):
        context = super(PostUpdateView, self).get_context_data(**kwargs)
        context['notifications'], context['unread_count'] = recieve(self.request.user)
        return context


@method_decorator([login_required, chief_required], name='dispatch')
class PostApprovalListView(ListView):
    '''
        View to list of all post that need to be approved.
    '''
    template_name = 'blog/post_approve_list.html'
    ordering = ('published_date',)
    context_object_name = 'postApprove'
    paginate_by = 10

    def get_queryset(self):
        return Post.objects.filter(is_approve=False)

    def get_context_data(self, **kwargs):
        context = super(PostApprovalListView, self).get_context_data(**kwargs)
        context['notifications'], context['unread_count'] = recieve(self.request.user)
        return context


@method_decorator([login_required, chief_required], name='dispatch')
class PostApprovalView(UpdateView):
    '''
        View create a form to approve the blog by moderator.
    '''
    template_name = 'blog/post_approval.html'
    model = Post
    fields = ['is_approve']
    # success_url = reverse_lazy('post_approve_list')

    def get_success_url(self):
        try:
            post = Post.objects.get(id=self.object.pk)
            if post.is_approve:
                verb = "%s Approved" % post.title
            else:
                verb = "%s Not Approved" % post.title
        except ObjectDoesNotExist:
            pass
        send(
            sender=self.request.user,
            recipient=User.objects.get(id=self.object.owner_id),
            verb=verb,
            blog_id=post.id
        )
        return reverse_lazy('post_approve_list')

    def get_context_data(self, **kwargs):
        context = super(PostApprovalView, self).get_context_data(**kwargs)
        context['notifications'], context['unread_count'] = recieve(self.request.user)
        return context


@method_decorator([login_required, chief_required], name='dispatch')
class PostApprovedView(ListView):
    '''
        View to show the approved post by loged in moderator
    '''
    template_name = 'blog/post_approve_list.html'
    ordering = ('published_date',)
    context_object_name = 'postApprove'
    paginate_by = 10

    def get_queryset(self):
        return Post.objects.filter(is_approve=True)

    def get_context_data(self, **kwargs):
        context = super(PostApprovedView, self).get_context_data(**kwargs)
        context['notifications'], context['unread_count'] = recieve(self.request.user)
        return context


@method_decorator([login_required], name='dispatch')
class PostDeleteView(DeleteView):
    model = Post
    success_url = reverse_lazy('post_list')
