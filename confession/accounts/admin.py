from django.contrib import admin
from confession.accounts.models import *
# Register your models here.


admin.site.register(User)
admin.site.register(Skill)
admin.site.register(Education)
admin.site.register(Experience)
admin.site.register(Project)
admin.site.register(Languages)
admin.site.register(Interest)
admin.site.register(Awards)
