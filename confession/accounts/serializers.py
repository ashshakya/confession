from django.contrib.auth import authenticate, get_user_model
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from rest_framework_jwt.settings import api_settings

from confession.accounts.models import (
    Awards,
    Education,
    Experience,
    Interest,
    Languages,
    Project,
    Skill
)
from confession.utils import timezone, try_or


UserModel = get_user_model()

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


class TokenSerializer(serializers.Serializer):
    """
    This serializer serializes the token data
    """

    user_id = serializers.IntegerField(required=False)
    username = serializers.CharField(max_length=255)
    token = serializers.CharField(max_length=255, required=False)
    success = serializers.BooleanField(default=False)


# Custom Validator for password.
def password_validate(password):
    """
    Validate Password.
    """
    if not password:
        raise serializers.ValidationError(
            {"password": "Password cannot be empty!"}
        )
    elif len(password) < 8:
        raise serializers.ValidationError({"password": "Password too short."})


class SkillSerializer(serializers.ModelSerializer):
    """
    This serializer serializes the skill model
    """

    def create(self, validated_data):
        skill = Skill.objects.create(**validated_data)
        return skill

    def update(self, instance, validated_data):
        instance.user_id = validated_data.get("user", instance.user_id)
        instance.skill = validated_data.get("skill", instance.skill)
        instance.rating = validated_data.get("rating", instance.rating)
        instance.save()
        return instance

    class Meta:
        model = Skill
        fields = (
            "skill",
            "rating",
        )
        write_only_fields = ("user",)


class EducationSerializer(serializers.ModelSerializer):
    """
    This serializer serializes the Education model
    """

    def create(self, validated_data):
        education = Education.objects.create(**validated_data)
        return education

    class Meta:
        model = Education
        fields = (
            "qualification",
            "specialization",
            "university",
            "grade",
            "start_year",
            "end_year",
            "achievement",
        )
        depth = 1


class ExperienceSerializer(serializers.ModelSerializer):
    """
    This serializer serializes the experience model
    """

    start_date = serializers.DateField(format="%m-%Y")
    end_date = serializers.DateField(format="%m-%Y")

    def create(self, validated_data):
        experience = Experience.objects.create(**validated_data)
        return experience

    class Meta:
        model = Experience
        fields = (
            "designation",
            "org_name",
            "start_date",
            "end_date",
            "description"
        )


class ProjectSerializer(serializers.ModelSerializer):
    """
    This serializer serializes the project model
    """

    start_date = serializers.DateField(format="%m-%Y")
    end_date = serializers.DateField(format="%m-%Y")

    def create(self, validated_data):
        project = Project.objects.create(**validated_data)
        return project

    class Meta:
        model = Project
        fields = (
            "title",
            "description",
            "start_date",
            "end_date",
        )


class LanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Languages
        fields = (
            "language",
            "rating",
        )


class InterestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Interest
        fields = ("interest",)


class AwardsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Awards
        fields = ("award",)


class UserSerializer(serializers.ModelSerializer):

    email_check = UniqueValidator(
        queryset=UserModel.objects.all(), message="Email already exists"
    )
    first_name = serializers.CharField(
        max_length=128, help_text="String contains the first name"
    )
    middle_name = serializers.CharField(
        max_length=128, required=False,
        help_text="String contains the last name"
    )
    last_name = serializers.CharField(
        max_length=128, required=False,
        help_text="String contains the last name"
    )
    full_name = serializers.SerializerMethodField()
    email = serializers.EmailField(
        required=True,
        max_length=100,
        validators=[email_check],
        help_text="A unique email",
    )
    address = serializers.SerializerMethodField()
    skill = SkillSerializer(many=True, source="skill_set", required=False)
    experience = ExperienceSerializer(
        many=True, source="experience_set", required=False
    )
    education = EducationSerializer(
        many=True, source="education_set", required=False
    )
    project = ProjectSerializer(
        many=True, source="project_set", required=False
    )
    languages = LanguageSerializer(
        many=True, source="language_set", required=False
    )
    interests = InterestSerializer(
        many=True, source="interest_set", required=False
    )
    awards = AwardsSerializer(many=True, source="award_set", required=False)

    def create(self, validated_data):
        user = UserModel.objects.create_user(**validated_data)
        return user

    class Meta:
        model = UserModel
        fields = (
            "email",
            "first_name",
            "middle_name",
            "last_name",
            "full_name",
            "avatar",
            "title",
            "introduction",
            "bio",
            "mobile",
            "address",
            "gender",
            "dob",
            "resume",
            "token",
            "skill",
            "order",
            "education",
            "experience",
            "project",
            "languages",
            "interests",
            "awards",
        )
        read_only_fields = (
            "token",
            "is_staff",
            "is_active",
        )

    def get_full_name(self, obj):
        return obj.get_full_name()

    def get_address(self, obj):
        city = try_or(lambda: obj.address.city.city_name, "")
        state = try_or(lambda: obj.address.city.state.state_name, "")
        return f"{city}, {state}"


class RegisterSerializer(UserSerializer):
    """
    Exact copy of the UserSerializer, the only difference being that this
    exposes the token of the user.
    """

    class Meta(UserSerializer.Meta):
        extra_fields = ("token",)
        read_only_fields = ("token",)

    def get_field_names(self, declared_fields, info):
        expanded_fields = super().get_field_names(declared_fields, info)

        if getattr(self.Meta, "extra_fields", None):
            return expanded_fields + self.Meta.extra_fields
        return expanded_fields


class LoginSerializer(serializers.Serializer):
    email = serializers.CharField(max_length=100, required=True)
    password = serializers.CharField(
        max_length=100,
        write_only=True,
        validators=[password_validate],
        help_text="Password having greater than 8 character",
    )

    token = serializers.CharField(max_length=255, read_only=True)
    user_type = serializers.IntegerField(read_only=True)
    first_name = serializers.CharField(read_only=True)
    middle_name = serializers.CharField(read_only=True)
    last_name = serializers.CharField(read_only=True)
    is_active = serializers.CharField(read_only=True)

    def validate(self, data):
        email = data.get("email", None)
        password = data.get("password", None)

        if email is None:
            raise serializers.ValidationError(
                "Email/mobile is required to log in."
            )

        if password is None:
            raise serializers.ValidationError(
                "Password is required to log in."
            )

        # The `authenticate` method is provided by Django and handles checking
        # for a user that matches this email/password combination.
        user = authenticate(username=email, password=password)

        if user is None:
            raise serializers.ValidationError(
                "A user with this email and password not found."
            )

        if not user.is_active:
            raise serializers.ValidationError("The user has been deactivated.")

        user.last_login = timezone.now_local()
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        user.save(update_fields=["last_login"])

        return {
            "email": user.email,
            "token": token,
            "user_type": user.user_type,
            "first_name": user.first_name,
            "middle_name": user.middle_name,
            "last_name": user.last_name,
            "is_active": user.is_active,
        }

    def create(self, validated_data):
        return UserModel(**validated_data)
