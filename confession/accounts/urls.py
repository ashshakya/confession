from django.urls import path

from confession.accounts.views import (
    ConnectView,
    LoginView,
    RegisterationView,
    UserProfileView
)

urlpatterns = [
    path('register/', RegisterationView.as_view(), name='register'),
    path('login/', LoginView.as_view(), name='login'),
    path('profile/<int:pk>', UserProfileView.as_view(), name='user_profile'),
    path('connect', ConnectView.as_view(), name='connect'),
]
