"""module API view for accounts APP."""

from django.conf import settings
from django.shortcuts import get_object_or_404
from django.contrib.auth import get_user_model
from rest_framework import permissions
from rest_framework_jwt.settings import api_settings
from rest_framework.views import APIView

from confession.accounts.serializers import (
    LoginSerializer,
    RegisterSerializer,
    UserSerializer,
)
from confession.base import response
from confession.utils import email

JWT_PAYLOAD_HANDLER = api_settings.JWT_PAYLOAD_HANDLER
JWT_ENCODE_HANDLER = api_settings.JWT_ENCODE_HANDLER
JWT_DECODE_HANDLER = api_settings.JWT_DECODE_HANDLER

UserModel = get_user_model()


class RegisterationView(APIView):
    """
    API to create a new user.

    endpoints: api/v1/accounts/register/
    Method: POST
    Args:
        email: Unique email
        password: Password with 8 or more character
        first_name: First Name
        last_name: Last Name
    Returns:
        username: string
        token: string
        success: bool
    """

    permission_classes = (permissions.AllowAny,)
    serializer_class = RegisterSerializer

    def post(self, request):
        """HTTP method to create register."""
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return response.Ok(serializer.data)


class LoginView(APIView):
    """
    API to create a new user.
    ```
        endpoints: api/v1/accounts/login/
        Method: POST
        Args:
            email: Unique email
            password: Password with 8 or more character
        Returns:
            username: string
            token: string
            success: bool
    ```
    """

    permission_classes = (permissions.AllowAny,)
    serializer_class = LoginSerializer

    def post(self, request):
        """
        function to login a user
        """
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        return response.Ok(serializer.data)


class UserProfileView(APIView):
    """
    API to fetch user profile.
    ```
        endpoints: api/v1/accounts/profile/<pk>
        Method: GET
        Returns:
            email: string
            first_name: string
            mobile: string
            address: string
    ```
    """

    permission_classes = (permissions.AllowAny,)
    serializer_class = UserSerializer

    def get(self, request, pk):
        """
        Get user profile
        """
        user = get_object_or_404(UserModel, pk=pk)
        serializer = self.serializer_class(user, context={"request": request})
        return response.Ok(serializer.data)


class ConnectView(APIView):
    """
    API to send message who want to connect with me.
    ```
        endpoints: api/v1/accounts/connect>
        Method: POST
        Args:
            email: string
            first_name: string
            subject: string
            body: string
        Returns:
            status: bool
    ```
    """

    permission_classes = (permissions.AllowAny,)

    def post(self, request):
        name = request.data.get("name")
        client_email = request.data.get("email")
        subject = request.data.get("subject")
        body = request.data.get("body")

        if not (name and client_email and subject and body):
            return response.BadRequest({"status": False})

        html_body = f"""
        Hi Team,<br>
        {name} and his email is <b>{client_email}</b>
        is trying to contact you for the reason: {body}
        """
        status = email.send(
            to=settings.MAILER_INBOX,
            subject=subject,
            html_body=html_body,
            cc=settings.MAILER_CC,
        )

        return response.Ok({"status": status})
