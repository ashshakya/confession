import datetime

from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from django.contrib.auth.models import (
    AbstractBaseUser, PermissionsMixin
)
# from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from simple_history.models import HistoricalRecords
from rest_framework_jwt.settings import api_settings

from confession.data.models import Address, document_upload
from confession.accounts.managers import UserManager
from confession.base.models import TimeStampModel
from confession.base.validators.form_validators import file_extension_validator
from confession.utils import try_or

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


YEAR_CHOICES = [(r, r) for r in range(1950, datetime.date.today().year + 5)]
month_list = [
    'January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'
]
MONTH_CHOICES = [(index, i) for index, i in enumerate(month_list, start=1)]
RATING_CHOICES = [(r, r) for r in range(0, 101)]


class GenderType:
    MALE = 1
    FEMALE = 2


class UserTypes:
    MODERATOR = 1
    EDITOR = 2


class EducationalQualification:
    BELOW_HIGH_SCHOOL = 1
    HIGH_SCHOOL = 2
    INTERMEDIATE = 3
    GRADUATE = 4
    POSTGRADUATE = 5
    OTHER = 6


class User(AbstractBaseUser, PermissionsMixin, TimeStampModel):
    GENDER_CHOICES = (
        (None, 'Please select the gender.'),
        (GenderType.MALE, 'Male'),
        (GenderType.FEMALE, 'Female')
    )
    USERTYPE_CHOICES = (
        (None, 'Select User Type'),
        (UserTypes.MODERATOR, 'Moderator'),
        (UserTypes.EDITOR, 'Editor')
    )

    username = models.CharField(
        max_length=128, blank=True, null=True,
        unique=True, db_index=True
    )

    first_name = models.CharField(
        max_length=128, blank=True, null=True, default=''
    )
    middle_name = models.CharField(
        max_length=128, blank=True, null=True, default=''
    )
    last_name = models.CharField(
        max_length=128, blank=True, null=True, default=''
    )
    email = models.EmailField(
        max_length=256, blank=True, null=True, db_column='email',
        unique=True, db_index=True
    )
    avatar = models.ImageField(
        upload_to=document_upload, blank=True
    )
    title = models.CharField(max_length=100, blank=True, null=True)
    introduction = models.CharField(max_length=1000, blank=True, null=True)
    bio = RichTextUploadingField(blank=True, null=True)

    mobile = models.CharField(
        max_length=13, blank=True, null=True, db_index=True
    )
    gender = models.PositiveSmallIntegerField(
        choices=GENDER_CHOICES, null=True, blank=True
    )
    dob = models.DateField(null=True, blank=True)

    user_type = models.PositiveSmallIntegerField(
        choices=USERTYPE_CHOICES, blank=True, null=True,
        default=UserTypes.EDITOR
    )
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    address = models.OneToOneField(
        Address, null=True, blank=True, default=None,
        related_name='primary_address', on_delete=models.DO_NOTHING
    )

    resume = models.FileField(
        upload_to=document_upload,
        validators=[file_extension_validator],
        max_length=256, blank=True, null=True
    )

    objects = UserManager()

    # Email address to be used as the username
    USERNAME_FIELD = 'email'

    history = HistoricalRecords(
        history_change_reason_field=models.TextField(null=True)
    )

    def __str__(self):
        """
            Returns the email of the User when it is printed in the console
        """
        return f'{self.email}'

    @property
    def token(self):
        """
            Allows us to get a user's token by calling `user.token` instead of
            `user.generate_jwt_token().

            The `@property` decorator above makes this possible. `token` is
            called a 'dynamic property'.
        """
        return self._generate_jwt_token()

    def _generate_jwt_token(self):
        """
            Generates a JSON Web Token that stores this user's ID and has an
            expiry date set to 365 days into the future.
        """

        payload = jwt_payload_handler(self)
        token = jwt_encode_handler(payload)
        return token

    def get_full_name(self):
        """
            Returns the full name of the user.
        """
        full_name = self.first_name
        if self.middle_name:
            full_name = f'{full_name} {self.middle_name}'
        if self.last_name:
            full_name = f'{full_name} {self.last_name}'
        return try_or(lambda: full_name.title(), '')

    def save(self, *args, **kwargs):
        self.is_active = True
        super().save(*args, **kwargs)  # Call the 'real' save() method.


class Languages(TimeStampModel):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='language_set'
    )
    language = models.CharField(max_length=255, blank=False, null=False)
    rating = models.IntegerField(
        choices=RATING_CHOICES, blank=False,
        null=False, default=0
    )


class Interest(TimeStampModel):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='interest_set'
    )
    interest = models.CharField(max_length=255, blank=False, null=False)


class Awards(TimeStampModel):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='award_set'
    )
    award = models.CharField(max_length=255, blank=False, null=False)


class Skill(TimeStampModel):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='skill_set'
    )
    skill = models.CharField(max_length=255, blank=False, null=False)
    rating = models.IntegerField(
        choices=RATING_CHOICES, blank=False,
        null=False, default=0
    )

    def __str__(self):
        return f'{self.user}-{self.skill}'

    class Meta:
        unique_together = (('skill', 'user'),)


class Education(TimeStampModel):

    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='education_set'
    )
    qualification = models.CharField(
        max_length=255, blank=True, null=True, default=None
    )
    university = models.CharField(max_length=255, null=True, blank=True)
    specialization = models.CharField(max_length=255, null=True, blank=True)
    grade = models.CharField(max_length=10, blank=True, null=True)
    start_year = models.IntegerField(_('from_year'), choices=YEAR_CHOICES)
    end_year = models.IntegerField(_('end_year'), choices=YEAR_CHOICES)
    achievement = models.TextField(blank=True)

    def __str__(self):
        return f'{self.user}-{self.qualification}'


class Experience(TimeStampModel):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='experience_set'
    )
    designation = models.CharField(max_length=255, null=False)
    org_name = models.CharField(max_length=255, null=True)
    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)
    description = RichTextUploadingField(blank=True)

    def __str__(self):
        return f'{self.user}-{self.designation}'


class Project(TimeStampModel):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='project_set'
    )
    order = models.PositiveSmallIntegerField()
    title = models.CharField(max_length=128, blank=True, null=True)
    description = RichTextUploadingField(blank=True, null=True)
    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)

    class Meta:
        ordering = ['order']

    def __str__(self):
        return f'{self.order} - {self.title}'
