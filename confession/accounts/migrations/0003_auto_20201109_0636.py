# Generated by Django 2.2.13 on 2020-11-09 06:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_auto_20200713_0842'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicaluser',
            name='title',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='user',
            name='title',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
