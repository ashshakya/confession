from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.db import transaction

from confession.accounts.models import *


class LoginForm(forms.Form):
    email = forms.CharField(label='Email', max_length=100)
    password = forms.CharField(label='Password', widget=forms.PasswordInput())


class EditorSignUpForm(UserCreationForm):
    first_name = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Enter First Name'}),
        strip=True
    )
    last_name = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Enter Last Name'}),
        strip=True
    )
    email = forms.EmailField(
        widget=forms.EmailInput(attrs={'placeholder': 'Enter Email'})
    )

    class Meta(UserCreationForm.Meta):
        model = User
        widgets = {
            'email': forms.TextInput(attrs={'placeholder': 'email'}),
            'password1': forms.PasswordInput(attrs={'placeholder': 'Password'}),
            'password2': forms.PasswordInput(attrs={'placeholder': 'Password'}),

        }

    @transaction.atomic
    def save(self, commit=True):
        user = super().save(commit=False)
        user.is_editor = True
        user.set_password(self.cleaned_data["password1"])
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.email = self.cleaned_data['email']
        if commit:
            user.save()
        return user


class ChiefSignUpForm(UserCreationForm):
    first_name = forms.CharField(widget=forms.TextInput(), strip=True)
    last_name = forms.CharField(widget=forms.TextInput(), strip=True)
    email = forms.EmailField(widget=forms.EmailInput())

    class Meta(UserCreationForm.Meta):
        model = User

    def save(self, commit=True):
        user = super().save(commit=False)
        user.is_chief = True
        user.set_password(self.cleaned_data["password1"])
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.email = self.cleaned_data['email']
        if commit:
            user.save()
        return user


class UserInfoForm(forms.ModelForm):

    class Meta:
        model = User
        fields = (
            'first_name', 'middle_name', 'last_name', 'avatar',
            'introduction', 'bio', 'mobile', 'gender', 'dob'
        )


class SkillsForm(forms.ModelForm):

    class Meta:
        model = SkillSet
        fields = ('skill', 'rating',)
        widgets = {
            'skill': forms.TextInput(attrs={"placeholder": "eg. Java, Python etc"}),
            'rating': forms.TextInput(attrs={'type': 'range', 'step': '1'})
        }


# SkillsFormset = formset_factory(SkillsForm, extra=1)
# SkillsFormset = inlineformset_factory(SkillSet, fields=['skill', 'rating'], extra=1)


class QualificationForm(forms.ModelForm):

    class Meta:
        model = Qualification
        fields = ('qualification', 'specialization', 'grade', 'from_year', 'completion_year', 'achievement',)


class ExperienceForm(forms.ModelForm):

    class Meta:
        model = Experience
        exclude = ('user',)


class ContactForm(forms.Form):
    name = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={'placeholder': 'Name', 'class': 'form-control'}),
    )
    from_email = forms.EmailField(
        required=True,
        widget=forms.EmailInput(attrs={'placeholder': 'Email', 'class': 'form-control'})
    )
    subject = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={'placeholder': 'Subject', 'class': 'form-control'})
    )
    message = forms.CharField(
        required=True,
        widget=forms.Textarea(attrs={'placeholder': 'Message', 'class': 'form-control'}),
    )
