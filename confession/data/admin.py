from django.contrib import admin

from confession.data.models import (
    Address, City, State
)

admin.site.register(Address)


@admin.register(State)
class StateAdmin(admin.ModelAdmin):
    list_display = ('id', 'state_name',)


@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    list_display = ('id', 'city_name', 'state', 'is_active')


# class DocumentAdmin(admin.ModelAdmin):
#     model = Documents
#     list_display = [
#         'document_category', 'doc_type', 'version', 'release_date'
#     ]

#     class Media:
#         js = (
#             '/static/admin/js/assets_admin.js',
#         )
