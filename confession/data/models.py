from django.conf import settings
from django.db import models

from confession.utils import timezone
from confession.base.models import TimeStampModel


def document_upload(instance, filename):
    """
    Stores the attachment in a 'per private-documents/module-type/yyyy/mm/dd' folder.
    :param instance, filename
    :returns ex: private-documents/personalimage/2020/03/10/filename
    """
    today = timezone.get_today_start()
    return 'private-documents/{model}/{year}/{month}/{day}/{filename}'.format(
        model=instance._meta.model_name,
        year=today.year, month=today.month,
        day=today.day, filename=filename,
    )


def file_upload(instance, filename):
    """
    Stores the attachment in a 'per private-documents/module-type/yyyy/mm/dd' folder.
    :param instance, filename
    :returns ex: private-documents/personalimage/2020/03/10/filename
    """
    today = timezone.get_today_start()
    return 'transaction-documents/{model}/{year}/{month}/{day}/{filename}'.format(
        model=instance._meta.model_name, year=today.year, month=today.month,
        day=today.day, filename=filename,
    )


def upload_image(instance, image):
    """
    Stores the attachment in a 'per confession-gallery/module-type/yyyy/mm/dd' folder.
    :param instance, filename
    :returns ex: confession-gallery/User-profile/2016/03/30/filename
    """
    today = timezone.get_today_start()
    return 'confession-gallery/{model}/{year}/{month}/{day}/{pk}_{image}'.format(
        model=instance._meta.model_name,
        year=today.year, month=today.month,
        day=today.day, pk=instance.pk, image=image,
    )


class State(TimeStampModel):
    state_name = models.CharField(max_length=128, blank=True, null=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return f'{self.state_name}'


class City(TimeStampModel):
    city_name = models.CharField(max_length=128, blank=True, null=True)
    state = models.ForeignKey(
        State, blank=True, null=True, default=None,
        on_delete=models.PROTECT
    )
    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name_plural = 'Cities'

    def __str__(self):
        return f'{self.city_name}'


class Address(TimeStampModel):
    address_line1 = models.CharField(max_length=128, blank=True, null=True)
    address_line2 = models.CharField(max_length=128, blank=True, null=True)
    address_line3 = models.CharField(max_length=128, blank=True, null=True)
    city = models.ForeignKey(
        City, blank=True, null=True, default=None, on_delete=models.PROTECT
    )
    pincode = models.CharField(max_length=6, blank=True, null=True)

    class Meta:
        verbose_name_plural = 'Addresses'

    def get_full_address(self):
        address = self.address_line1
        if self.address_line2:
            address = '{} {}'.format(address, self.address_line2)
            if self.address_line3:
                address = '{} {}'.format(address, self.address_line3)
        return address
