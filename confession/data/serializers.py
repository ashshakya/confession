from rest_framework import serializers

from confession.data.models import Address, City, State


class StateSerializer(serializers.ModelSerializer):

    class Meta:
        model = State
        fields = ('state_name',)
        read_only_fields = ('state_name',)


class CitySerializer(serializers.ModelSerializer):
    city = serializers.CharField(source='city_name')
    state = StateSerializer(read_only=True)

    class Meta:
        model = City
        fields = ('id', 'city', 'state')
        read_only_fields = ('id', 'city', 'state')


class AddressSerializer(serializers.ModelSerializer):
    pincode = serializers.CharField(
        max_length=6, min_length=6, required=False,
        allow_null=True, allow_blank=True, default=None,
        error_messages={
            'min_length': 'Pin code length must be 6 digits.',
            'max_length': 'Pin code length must be 6 digits.',
        }
    )
    address_line1 = serializers.CharField(
        max_length=128, required=False,
        allow_null=True, allow_blank=True, default=None,
        error_messages={
            'max_length': 'Address length must be 128 characters.',
        }
    )
    address_line2 = serializers.CharField(
        max_length=128, required=False,
        allow_null=True, allow_blank=True, default=None,
        error_messages={
            'max_length': 'Address length must be 128 characters.',
        }
    )
    address_line3 = serializers.CharField(
        max_length=128, required=False,
        allow_null=True, allow_blank=True, default=None,
        error_messages={
            'max_length': 'Address length must be 128 characters.',
        }
    )
    city = CitySerializer(read_only=True)
    city_id = serializers.PrimaryKeyRelatedField(
        queryset=City.objects.all(), write_only=True, source='city'
    )

    class Meta:
        model = Address
        fields = ('id', 'address_line1', 'address_line2',
                  'address_line3', 'pincode', 'city', 'city_id')
