from django.contrib import admin
from confession.base.admin import ChangedDataSimpleHistoryAdmin

from confession.pubsub.models import Subscribers


@admin.register(Subscribers)
class SubscribersAdmin(ChangedDataSimpleHistoryAdmin):
    list_display = ('email', 'is_active', 'product')
    search_fields = ('email',)
    list_filter = ('is_active', 'product')
    readonly_fields = ('created_at', 'modified_at',)
