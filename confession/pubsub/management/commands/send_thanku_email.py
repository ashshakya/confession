from django.core.management.base import BaseCommand

from confession.pubsub.models import Subscribers
from confession.utils import email


class Command(BaseCommand):
    help = 'Send Thank You Email to All active subscribers'

    def handle(self, *args, **options):
        subscribers = Subscribers.objects.filter(is_active=True)
        for user in subscribers:
            status = email.send_from_template(
                to=user.email,
                subject='Confession: Thank You for Subscribing.',
                template='thanku_subscriber_email.html',
                context={}
            )
            print(user.email, status)
