from django.urls import re_path

from confession.pubsub.views import SubscribeView


urlpatterns = [
    re_path('^subscribe/$', SubscribeView.as_view(), name='subscribe')
]
