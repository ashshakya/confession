from rest_framework import serializers

from confession.pubsub.models import Subscribers


class SubscriberSerializer(serializers.ModelSerializer):

    class Meta:
        model = Subscribers
        fields = (
            'email',
            'product',
            'is_active'
        )
