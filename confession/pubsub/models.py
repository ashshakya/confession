from django.db import models
from simple_history.models import HistoricalRecords

from confession.base.models import TimeStampModel


class PRODUCT_TYPE:
    BLOG = 1
    NEWSLETTER = 2


class Subscribers(TimeStampModel):
    PRODUCT_CHOICES = (
        (PRODUCT_TYPE.BLOG, 'Blog'),
        (PRODUCT_TYPE.NEWSLETTER, 'News Letter'),
    )

    email = models.EmailField(
        max_length=256, db_column='subscriber_email',
        unique=True, db_index=True
    )
    is_active = models.BooleanField(default=True)
    product = models.PositiveSmallIntegerField(
        choices=PRODUCT_CHOICES, null=True, blank=True, default=1
    )

    history = HistoricalRecords(
        history_change_reason_field=models.TextField(null=True)
    )

    def __str__(self):
        return f'{self.email}'
