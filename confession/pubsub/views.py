import logging

from rest_framework.views import APIView
from rest_framework import permissions

from confession.pubsub.models import Subscribers
from confession.pubsub.serializers import SubscriberSerializer
from confession.base import response
from confession.utils import email

logger = logging.getLogger(__name__)


class SubscribeView(APIView):

    permission_classes = (permissions.AllowAny,)
    serializer_class = SubscriberSerializer

    def get(self, request):
        try:
            subscriber = Subscribers.objects.get(
                email=request.data.get('email')
            )
            subscriber.is_active = False
            subscriber.save()
            return response.Ok()
        except Subscribers.DoesNotExist:
            return response.BadRequest({
                'error': 'Please send valid email.'
            })

    def post(self, request):
        try:
            subscriber = Subscribers.objects.get(
                email=request.data.get('email')
            )
            serializer = SubscriberSerializer(
                subscriber, request.data, partial=True
            )
        except Subscribers.DoesNotExist:
            serializer = SubscriberSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        try:
            email.send_from_template(
                to=request.data.get('email'),
                subject='Confession: Thank You for Subscribing.',
                template='thanku_subscriber_email.html',
                context={}
            )
        except Exception:
            logger.exception(
                'Unable to send subscriber email to user: %s',
                request.data.get('email')
            )
        return response.Ok(serializer.data)
