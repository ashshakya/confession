from django.core.files.base import File
from django.core.files.temp import NamedTemporaryFile
from django.http import HttpResponse
from django.template.loader import get_template
from html import escape
from io import BytesIO
from xhtml2pdf import pisa


def html_to_pdf_convert(template, context):
    html = template.render(context)
    policy_document_file = NamedTemporaryFile(delete=False)

    pdf = pisa.pisaDocument(BytesIO(html.encode('UTF-8')), policy_document_file)
    if not pdf.err:
        return File(policy_document_file)
    return False


def convert_html_to_pdf(template, context, document_owner):
    html = template.render(context)

    user = document_owner.get_full_name()
    f = NamedTemporaryFile()
    f.name = '/' + user + '.pdf'
    pdf = pisa.pisaDocument(BytesIO(html.encode('UTF-8')), f)

    if not pdf.err:
        return File(f)
    return False


def render_to_pdf(template_src, context_dict):
    template = get_template(template_src)
    html = template.render(context_dict)
    result = BytesIO()

    pdf = pisa.pisaDocument(BytesIO(html.encode('UTF-8')), result)
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type='application/pdf')
    return HttpResponse('We had some errors<pre>%s</pre>' % escape(html))
