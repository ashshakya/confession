import logging
import os
import os.path

from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags

logger = logging.getLogger(__name__)


def send(to, subject, html_body, text_body=None, attachments=[], from_email=None, cc=None, bcc=None):
    if not isinstance(to, (list, tuple)):
        to = [to]

    # Remove empty items
    to = [x for x in to if x not in (None, '')]

    if text_body is None:
        text_body = strip_tags(html_body)

    # Convert CC into a list
    if cc and not isinstance(cc, (list, tuple)):
        cc = [cc]

    # Convert BCC into a list
    if bcc and not isinstance(bcc, (list, tuple)):
        bcc = [bcc]

    # if bcc is None, set a default email as bcc
    # if not bcc:
    #     bcc = [settings.EMAIL_NOTIFICATIONS.get('BCC_CATCH_ALL_INTERNAL')]
    try:
        msg = EmailMultiAlternatives(subject, text_body, to=to)
        if cc:
            msg.cc = cc

        if bcc:
            msg.bcc = bcc

        if from_email:
            msg.from_email = from_email

        msg.attach_alternative(html_body, 'text/html')
        for attachment in attachments:
            if attachment:
                # Try to get only filename from full-path
                try:
                    attachment.open()
                except Exception as e:
                    print(str(e))
                attachment_name = os.path.split(attachment.name)[-1]
                try:
                    msg.attach(attachment_name or attachment.name, attachment.read())
                except UnicodeDecodeError:
                    msg.attach_file(attachment.name)
        msg.send()
        return True
    except Exception:
        logger.exception('Unable to send the mail.')
        return False


def send_from_template(to, subject, template, context, **kwargs):
    html_body = render_to_string(template, context)
    logger.info('Sending email to: %s', to)
    return send(to, subject, html_body, **kwargs)
