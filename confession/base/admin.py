from simple_history.admin import SimpleHistoryAdmin


class ChangedDataSimpleHistoryAdmin(SimpleHistoryAdmin):
    """
    Custom history settings for displaying changed fields and changed values
    """
    history_list_display = ['changed_fields', 'changed_values']

    def changed_fields(self, obj):
        """
        Function for getting the changed fields
        """
        if obj.prev_record:
            delta = obj.diff_against(obj.prev_record)
            return delta.changed_fields
        return None

    def changed_values(self, obj):
        """
        Function for getting the changed values with respective fields
        """
        changed_fields = self.changed_fields(obj) or []
        changed_values = {}
        for fields in changed_fields:
            changed_values.update({
                fields: {
                    'new': obj.__getattribute__(fields),
                    'old': obj.prev_record.__getattribute__(fields)
                }
            })
        return changed_values
