"""confession URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path
from django.conf import settings
from django.conf.urls.static import static

from ckeditor_uploader import views as uploader_views
from django.views.decorators.cache import never_cache


admin.site.site_header = 'Confession Administration'
admin.site.site_title = 'Confession Administration'
admin.site.index_title = 'Confession Administration'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/accounts/', include('confession.accounts.urls')),
    path('api/v1/blog/', include('confession.blog.apis.urls')),
    path('api/v1/pubsub/', include('confession.pubsub.urls')),

    re_path(
        r'^ckeditor/upload/',
        uploader_views.upload,
        name='ckeditor_upload'
    ),
    re_path(
        r'^ckeditor/browse/',
        never_cache(uploader_views.browse),
        name='ckeditor_browse'
    ),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [path('__debug__/', include(debug_toolbar.urls))]
